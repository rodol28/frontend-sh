import React from "react";
import { Spinner, Center } from "@chakra-ui/react";


export function CustomSpinner() {
  return (
    <Center height="60vh">
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="black"
        size="xl"
      />
    </Center>
  );
}
