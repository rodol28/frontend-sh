import React from "react";
import { Text } from "@chakra-ui/react";
import { Link } from "wouter";

export const CustomLink = ({ children, to }) => {
  return (
    <Link to={to}>
      <Text
        display="block"
        _focus={{ outline: "none", border: "none " }}
        _hover={{ outline: "none", cursor: "pointer", color: "#A0AEC0" }}
      >
        {children}
      </Text>
    </Link>
  );
};
