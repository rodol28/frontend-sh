import React from "react";
import { ButtonGroup, IconButton } from "@chakra-ui/react";
import { FaGitlab, FaLinkedin, FaTwitter } from "react-icons/fa";

export const SocialMediaLinks = (props) => (
  <ButtonGroup variant="ghost" color="gray.600" {...props}>
    <IconButton
      as="a"
      href="#"
      aria-label="LinkedIn"
      _focus={{ outline: "none", border: "none " }}
      icon={<FaLinkedin fontSize="20px" />}
    />
    <IconButton
      as="a"
      href="https://gitlab.com/rodol28/frontend-sh"
      target="_blank"
      aria-label="Gitlab"
      _focus={{ outline: "none", border: "none " }}
      icon={<FaGitlab fontSize="20px" />}
    />
    <IconButton
      as="a"
      href="#"
      aria-label="Twitter"
      _focus={{ outline: "none", border: "none " }}
      icon={<FaTwitter fontSize="20px" />}
    />
  </ButtonGroup>
);
