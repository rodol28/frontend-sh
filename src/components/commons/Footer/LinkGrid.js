import React from "react";
import { Box, SimpleGrid, Stack } from "@chakra-ui/react";
import { FooterHeading } from "./FooterHeading";
import { CustomLink } from "./CustomLink";
import { paths } from "config/paths";

export const LinkGrid = (props) => (
  <SimpleGrid columns={3} {...props}>
    <Box minW="130px">
      <FooterHeading mb="4">Secciones</FooterHeading>
      <Stack mr={4}>
        <CustomLink to={paths.main}>Inicio</CustomLink>
        <CustomLink to={paths.search}>Buscar mi hogar</CustomLink>
        <CustomLink to={paths.registerHouse}>Publicar un inmueble</CustomLink>
      </Stack>
    </Box>
    <Box minW="130px">
      <FooterHeading mb="4">Acerca de</FooterHeading>
      <Stack mr={4}>
        <CustomLink to={paths.members}>Integrantes</CustomLink>
        <CustomLink to={paths.contacts}>Contacto</CustomLink>
      </Stack>
    </Box>
    <Box minW="130px">
      <FooterHeading mb="4">Legal</FooterHeading>
      <Stack>
        <CustomLink to={paths.main}>Privacidad</CustomLink>
        <CustomLink to={paths.main}>Términos</CustomLink>
        <CustomLink to={paths.main}>Licencia</CustomLink>
      </Stack>
    </Box>
  </SimpleGrid>
);
