import React from "react";
import { Image } from "@chakra-ui/react";
import LogoImg from "assets/images/logo2.png";
import { Link } from "wouter";

export function Logo() {

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  };

  return (
    <Link href="/" onClick={scrollToTop}>
      <Image
        borderRadius="full"
        boxSize="100px"
        src={LogoImg}
        alt="Logo"
        _hover={{ cursor: "pointer" }}
      />
    </Link>
  );
}
