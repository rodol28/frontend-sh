import React from "react";
import {
  GoogleMap,
  Marker,
  withGoogleMap,
  withScriptjs,
} from "react-google-maps";

import { mapStyles } from "./mapStyles";

const Map = props => {

  const { coordinates } = props;

  return (
    <GoogleMap
      defaultZoom={props.zoom}
      defaultCenter={{
        lat: Number(coordinates.lat),
        lng: Number(coordinates.lng)
      }}
      options={{
        styles: mapStyles,
      }}
    >
      {props.isMarkerShown &&
        <Marker position={{
          lat: Number(coordinates.lat),
          lng: Number(coordinates.lng)
        }} />}
    </GoogleMap>
  );
};

export default withScriptjs(withGoogleMap(Map));
