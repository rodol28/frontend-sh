import React from "react";

import { Menu, MenuButton, MenuItem, MenuList, Avatar } from "@chakra-ui/react";

import { useDispatch } from "react-redux";
import { signOutAction } from "reducers/authSlice";
import { useLocation } from "wouter";
import { paths } from "config/paths";

export function Profile({ name, token }) {
  // eslint-disable-next-line
  const [_, setLocation] = useLocation();
  const dispatch = useDispatch();

  const goToMyAccount = () => {
    setLocation(paths.account);
  };

  const handleSubmit = () => {
    dispatch(signOutAction({ token }));
    setLocation(paths.main);
  };

  return (
    <Menu>
      <MenuButton>
        <Avatar name={name} />
      </MenuButton>
      <MenuList color="black">
        <MenuItem onClick={goToMyAccount}>Mi Cuenta</MenuItem>
        <MenuItem onClick={handleSubmit}>Cerrar sesión</MenuItem>
      </MenuList>
    </Menu>
  );
}
