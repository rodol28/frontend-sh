import React from "react";
import { Text, useColorModeValue } from "@chakra-ui/react";
import { Link } from "wouter";

export function MenuItem({ children, isLast, to = "/", ...rest }) {
  return (
    <Link href={to}>
      <Text
        display="block"
        as="h4"
        color={useColorModeValue("gray.600", "gray.400")}
        fontSize="md"
        fontWeight="semibold"
        textTransform="uppercase"
        letterSpacing="wider"
        {...rest}
        _focus={{ outline: "none", border: "none " }}
        _hover={{ outline: "none", cursor: "pointer", color: "#A0AEC0" }}
      >
        {children}
      </Text>
    </Link>
  );
}
