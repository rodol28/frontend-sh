import React from "react";
import { Logo } from "../commons/Logo";
import { NavBarContainer } from "./NavBarContainer";
import { MenuLinks } from "./MenuLinks";
import { MenuToggle } from "./MenuToggle";

export function NavBar(props) {
  const [isOpen, setIsOpen] = React.useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <NavBarContainer {...props}>
      <Logo
        w="100px"
        color={["white", "white", "primary.500", "primary.500"]}
        rounded="md"
      />
      <MenuToggle toggle={toggle} isOpen={isOpen} />
      <MenuLinks isOpen={isOpen} />
    </NavBarContainer>
  );
}
