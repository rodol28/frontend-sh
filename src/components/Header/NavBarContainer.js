import React from "react";
import { Flex } from "@chakra-ui/react";

export function NavBarContainer({ children, ...props }) {
  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      w="100%"
      mb={4}
      px={8}
      py={2}
      bg={["white", "white", "white", "white"]}
      color={["white", "white", "white", "white"]}
      position="sticky"
      top="0px"
      zIndex="200"
      {...props}
    >
      {children}
    </Flex>
  );
}
