import React from "react";
import { Box, Button, Stack } from "@chakra-ui/react";
import { useSelector } from "react-redux";
import { authSelector } from "reducers/authSlice";
import { paths } from "config/paths";
import { Profile } from "./Profile";
import { MenuItem } from "./MenuItem";

export function MenuLinks({ isOpen }) {
  const { isLoggedIn, user, token } = useSelector(authSelector);

  return (
    <Box
      display={{ base: isOpen ? "block" : "none", md: "block" }}
      flexBasis={{ base: "100%", md: "auto" }}
    >
      <Stack
        spacing={8}
        align="center"
        justify={["center", "space-between", "flex-end", "flex-end"]}
        direction={["column", "row", "row", "row"]}
        pt={[4, 4, 0, 0]}
      >
        <MenuItem to={paths.main} color={["black", "black", "black", "black"]}>
          Inicio
        </MenuItem>
        {/* <MenuItem to={paths.why} color={["black", "black", "black", "black"]}>
          ¿Porque Segundo Hogar?{" "}
        </MenuItem> */}
        <MenuItem
          to={paths.aboutUs}
          color={["black", "black", "black", "black"]}
        >
          Acerca de{" "}
        </MenuItem>
        {isLoggedIn ? null : (
          <MenuItem
            to={paths.register}
            color={["black", "black", "black", "black"]}
          >
            Registrarse{" "}
          </MenuItem>
        )}
        {isLoggedIn ? (
          <Profile name={user.name} token={token} />
        ) : (
          <MenuItem
            to={paths.login}
            color={["black", "black", "white", "white"]}
            isLast
          >
            <Button
              display="block"
              fontSize="md"
              fontWeight="semibold"
              textTransform="uppercase"
              letterSpacing="wider"
              size="sm"
              rounded="md"
              color={["white", "white", "primary.500", "primary.500"]}
              bg="black"
              _hover={{
                background: "#36393f",
              }}
              _focus={{
                outline: "none",
                border: "none ",
              }}
            >
              Iniciar Sesion
            </Button>
          </MenuItem>
        )}
      </Stack>
    </Box>
  );
}
