export function mapHouseToApi({ house }) {
  const mappedHouse = {
    compartir: 1,
    habitaciones: parseInt(house.bedrooms),
    banios: parseInt(house.bathrooms),
    tamanio: parseInt(house.size),
    valoracion: 5,
    estadoinmueble: 0,
    tipoinmueble_id: parseInt(house.typeHouse),
    propietario_id: 2,
    direccion: house.address,
    piso: house.floor,
    departamento: house.apartment,
    latitud: house.coordanates.lat,
    longitud: house.coordanates.lng,
  };

  return mappedHouse;
}
