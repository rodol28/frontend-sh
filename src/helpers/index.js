import { mapUserToApi } from "./mapUserToApi";
import { mapHouseToApi } from "./mapHouseToApi";
import { mapPublicationToApi } from "./mapPublicationToApi";

export const helpers = {
  mapUserToApi,
  mapHouseToApi,
  mapPublicationToApi,
};
