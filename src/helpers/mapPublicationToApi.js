export function mapPublicationToApi({ publication }) {
    const mappedPublication = {
      inmueble_id: publication.lastRegisteredHouse,
      mascotas: publication.pets ? 1 : 0,
      fumadores: publication.smokers ? 1 : 0,
      ninios: publication.children ? 1 : 0,
      cantidadinquilinos: parseInt(publication.amount),
      titulo: publication.title,
      descripcion: publication.description,
      precio: publication.price,
      esamoblado: parseInt(publication.isFurnished),
      nombrecontacto: publication.fullname,
      telefonocontacto: publication.phone,
      emailcontacto: publication.email,
      estadopublicacion: 1,
      fechadevigencia: new Date(),
      fecha: new Date(),
    };
  
    return mappedPublication;
  }
  