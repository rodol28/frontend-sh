export function mapUserToApi({ user }) {
  const mappedUser = {
    carrera_id: user.carreer,
    ciudad_id: user.city,
    fechanacimiento: user.dateOfBirth,
    email: user.email,
    nombre: user.firstname,
    sexo: user.gender,
    apellido: user.lastname,
    legajo: user.numberSumary,
    password: user.password,
    provincia_id: user.state,
    name: user.username,
  };

  return mappedUser;
}
