import search from "assets/svg/search.svg";
import roommate from "assets/svg/roommate.svg";
import post from "assets/svg/post.svg";

export const imagesText = [
  {
    id: 1,
    image: search,
    title: "Title number 1",
    from: "left",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque ipsam pariatur corrupti ipsum possimus, temporibus quaerat accusantium voluptas suscipit est sequi non nesciunt molestias quasi? Est harum accusantium quis id.",
  },
  {
    id: 2,
    image: roommate,
    title: "Title number 2",
    from: "right",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque ipsam pariatur corrupti ipsum possimus, temporibus quaerat accusantium voluptas suscipit est sequi non nesciunt molestias quasi? Est harum accusantium quis id.",
  },
  {
    id: 3,
    image: post,
    title: "Title number 3",
    from: "left",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque ipsam pariatur corrupti ipsum possimus, temporibus quaerat accusantium voluptas suscipit est sequi non nesciunt molestias quasi? Est harum accusantium quis id.",
  }
];
