import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "bootswatch/dist/materia/bootstrap.min.css";
import { ChakraProvider } from "@chakra-ui/react";
import store from "./app/store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
const persistor = persistStore(store);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ChakraProvider>
        <App />
      </ChakraProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);
