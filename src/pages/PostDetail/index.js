import React, { useEffect } from "react";
import { useRoute } from "wouter";
import { Box, Container, Flex, Center, Text, Heading } from "@chakra-ui/react";
import { StarIcon } from "@chakra-ui/icons";
import "swiper/swiper-bundle.min.css";

import { Slider } from "./components/Slider";
import { GoogleMapsContainer } from "components/GoogleMaps";

import { useDispatch, useSelector } from "react-redux";
import {
  fetchPublicationById,
  setCurrentPublication,
} from "reducers/publicationSlice";
import { paths } from "config/paths";

export function PostDetail() {
  // eslint-disable-next-line
  const [match, params] = useRoute(paths.postDetail);

  const dispatch = useDispatch();
  const post = useSelector((state) => state.publications.post);

  useEffect(() => {
    dispatch(fetchPublicationById({ idPost: params.id }));

    return () => setCurrentPublication();
  }, [dispatch, params.id]);

  if (post === null) return <h1>Loading...</h1>;

  return (
    <Container maxW="container.xl" mt={4}>
      <Slider />

      <Flex color="#000" mt={16}>
        <Flex w="300px" flexDir="column">
          <Heading size="lg">Detalle</Heading>
          <Text mt={4}>{post.address}</Text>
          <Text mt={2} color="blackAlpha.700">
            - Tipo: {post.typeHouse}
          </Text>
          <Text mt={2} color="blackAlpha.700">
            - Precio: ${post.price}
          </Text>
          <Text mt={2} color="blackAlpha.700">
            - Habitaciones: {post.bedrooms}
          </Text>
          <Text mt={2} color="blackAlpha.700">
            - Baños: {post.bathrooms}
          </Text>
          <Text mt={2} color="blackAlpha.700">
            - Tamaño (m2): {post.size}
          </Text>

          <Box d="flex" mt="2" alignItems="center">
            {Array(5)
              .fill("")
              .map((_, i) => (
                <StarIcon
                  key={i}
                  color={i < post.rating ? "teal.500" : "gray.300"}
                />
              ))}
            <Box as="span" ml="2" color="gray.600" fontSize="sm">
              {post.reviews} valoraciones
            </Box>
          </Box>
        </Flex>

        <Flex flexDir="column">
          <Box flex="1">
            <Heading size="lg">{post.title}</Heading>
            <Text mt={4} color={"#444"} textAlign="justify">
              {post.description}
            </Text>
          </Box>
          <Center my={8}>
            <Box height={"400px"} width={"600px"}>
              <GoogleMapsContainer
                initialCenter={post.coordinates}
                zoom={15}
                isMarkerShown={true}
                coordinates={post.coordinates}
              />
            </Box>
          </Center>
        </Flex>
      </Flex>
    </Container>
  );
}
