import { Swiper, SwiperSlide } from "swiper/react";
import { Box, Image } from "@chakra-ui/react";
import "swiper/swiper-bundle.min.css";

import { imagesTest } from "../images";

export function Slider() {
  return (
    <Box bg={"blackAlpha.300"}>
      <Swiper
        spaceBetween={50}
        slidesPerView={3}
        onSlideChange={() => console.log("slide change")}
        onSwiper={swiper => "console.log(swiper)"}
      >
        <SwiperSlide>
          <Box boxSize="sm">
            <Image
              src={imagesTest.image1}
              boxSize={["100px", "300px", "400px", "400px", "400px"]}
              objectFit="cover"
              alt="Foto departamento"
            />
          </Box>
        </SwiperSlide>

        <SwiperSlide>
          <Box boxSize="sm">
            <Image
              src={imagesTest.image2}
              boxSize={["100px", "300px", "400px", "400px", "400px"]}
              objectFit="cover"
              alt="Foto departamento"
            />
          </Box>
        </SwiperSlide>

        <SwiperSlide>
          <Box boxSize="sm">
            <Image
              src={imagesTest.image3}
              boxSize={["100px", "300px", "400px", "400px", "400px"]}
              objectFit="cover"
              alt="Foto departamento"
            />
          </Box>
        </SwiperSlide>

        <SwiperSlide>
          <Box boxSize="sm">
            <Image
              src={imagesTest.image4}
              boxSize={["100px", "300px", "400px", "400px", "400px"]}
              objectFit="cover"
              alt="Foto departamento"
            />
          </Box>
        </SwiperSlide>
      </Swiper>
    </Box>
  );
}
