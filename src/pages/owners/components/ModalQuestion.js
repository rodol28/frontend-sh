import React from "react";
import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalCloseButton,
} from "@chakra-ui/react";
import { useLocation } from "wouter";
import { paths } from "config/paths";

export function ModalQuestion({ isOpen, onClose }) {
  // eslint-disable-next-line
  const [_, setLocation] = useLocation();

  const handleClick = op => {
    onClose();
    if (op === "goToAccount") {
      setLocation(paths.main);
    } else {
      setLocation(paths.registerPost);
    }
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>El inmueble se registro exitosamente</ModalHeader>
        <ModalCloseButton />
        <ModalFooter d="flex" justifyContent="center">
          <Button variant="ghost" onClick={() => handleClick("goToAccount")}>
            Ir a mi cuenta
          </Button>
          <Button
            colorScheme="blue"
            mr={3}
            onClick={() => handleClick("goToPostHouse")}
          >
            Publicar el inmueble
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
