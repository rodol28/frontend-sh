import React from "react";
import { FormArea } from "pages/owners/components/FormArea";
import { RegisterHouseForm } from "./components/RegisterHouseForm";
import { SeccionHeader } from "components/SeccionHeader";
import { sections } from "config/sections";

export function RegisterHousePage() {
  const { registerHouse } = sections;

  return (
    <FormArea>
      <SeccionHeader section={registerHouse.section} sectionTitle={registerHouse.title} />
      <RegisterHouseForm />
    </FormArea>
  );
}
