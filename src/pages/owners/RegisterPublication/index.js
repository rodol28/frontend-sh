import React from "react";
import { FormArea } from "pages/owners/components/FormArea";
import { RegisterPublicationForm } from "./components/RegisterPublicationForm";
import { SeccionHeader } from "components/SeccionHeader";
import { sections } from "config/sections";

export function RegisterPublicationPage() {
  const { registerPublication } = sections;

  return (
    <FormArea>
      <SeccionHeader
        section={registerPublication.section}
        sectionTitle={registerPublication.title}
      />
      <RegisterPublicationForm />
    </FormArea>
  );
}
