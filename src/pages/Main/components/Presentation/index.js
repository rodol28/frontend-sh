import React from "react";
import { useLocation } from "wouter";

// styles

import styles from "./Presentation.module.css";

import { paths } from "config/paths";

// component Presentation

export default function Presentation() {
  // eslint-disable-next-line
  const [_, setLocation] = useLocation();

  const handleSearch = () => {
    setLocation(paths.search);
  };

  const handlePost = () => {
    setLocation(paths.registerHouse);
  };

  return (
    <>
      <div className="row d-flex justify-content-center">
        <h1 className={styles.presentation_title}>
          Tu sitio para encontrar tu hogar
        </h1>
        <p className={styles.presentation_description}>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Assumenda
          tempora expedita illo harum amet voluptate blanditiis illum quibusdam,
          quod ipsum!
        </p>
      </div>

      <div className={`${styles.container_btn}`}>
        <button
          onClick={handleSearch}
          className={`${styles.btn_find_home} ${styles.presentation_btn}`}
        >
          <i className={`${styles.presentation_btn_icon} fas fa-search`} />
          Buscar mi hogar
        </button>

        <button
          onClick={handlePost}
          className={`${styles.btn_post_apartment} ${styles.presentation_btn}`}
        >
          Publicar un inmueble
        </button>
      </div>
    </>
  );
}
