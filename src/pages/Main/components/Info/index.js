import React from "react";
import styles from "./Info.module.css";
import { useAnimate } from "hooks/useAnimate";
import { imagesText } from "const";

export default function Info() {
  useAnimate();

  return (
    <div>
      <div className="container text-center">
        {imagesText.map(item => {
          return (
            <div
              className={`${styles.custom_container} row d-flex flex-row`}
              key={item.id}
              id="load-animate"
            >
            
              <div className="col-lg-6">
                <img src={item.image} alt="" className={styles.image_info} />
              </div>

              <div className="col-lg-6">
                <p className={styles.title_info}>{item.title}</p>
                <p className={styles.text_info}>{item.text}</p>
              </div>

            </div>
          );
        })}
      </div>
    </div>
  );
}
