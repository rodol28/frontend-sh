import React from "react";

// components
import Presentation from "./components/Presentation";
import Info from "./components/Info";
// styles

import styles from "./Main.module.css";

// component Main

export function Main() {
  return (
    <div className="">
      <div className="container">
        <Presentation />
        <div className={styles.space} />
        <Info />
      </div>
    </div>
  );
}
