import React, { useEffect } from "react";
import { Box, Center } from "@chakra-ui/react";

/*-----------COMPONENTS-----------*/

import { Results } from "./components/Result";
import { FormArea } from "./components/FormArea";

/*-----------STATE-----------*/
import { useDispatch, useSelector } from "react-redux";
import { fetchAllPublications } from "reducers/publicationSlice";
import { publicationSelector } from "reducers/publicationSlice";
import { CustomSpinner } from "components/commons/CustomSpinner";

export function Search() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAllPublications());
  }, [dispatch]);

  const { posts } = useSelector(publicationSelector);

  if (posts.length === 0) return <CustomSpinner />;

  return (
    <>
      <FormArea posts={posts} />
      <Box width={"100%"} my={20}>
        <Center>
          <Results posts={posts} />
        </Center>
      </Box>
    </>
  );
}
