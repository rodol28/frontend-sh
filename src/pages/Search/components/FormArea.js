import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { GoogleMapsContainer } from "./GoogleMaps";
import { SearchForm } from "./SearchForm";
import { SeccionHeader } from "components/SeccionHeader";
import { sections } from "config/sections";

const initialCenter = { lat: -26.816683901060273, lng: -65.19854533333803 };

export function FormArea({ posts }) {
  const { search } = sections;

  return (
    <Flex
      flexDir={["column", "column", "row", "row"]}
      minHeight="440px"
      justifyContent="center"
      mx={8}
      borderWidth={1}
      px={2}
      borderRadius={4}
      textAlign="center"
      boxShadow="lg"
    >
      <Box width={["100%", "100%", "80%", "45%"]} pr={8} py={4}>
        <GoogleMapsContainer
          initialCenter={initialCenter}
          zoom={15}
          isMarkerShown={true}
          posts={posts}
        />
      </Box>
      <Box width={["100%", "100%", "80%", "45%"]} pl={8} py={4}>
        <SeccionHeader section={search.section} sectionTitle={search.title} />
        <SearchForm />
      </Box>
    </Flex>
  );
}
