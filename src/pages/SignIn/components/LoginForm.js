import React from "react";
import {
  Box,
  Link as LinkChakra,
  FormControl,
  FormLabel,
  FormErrorMessage,
  InputGroup,
  Input,
  InputRightElement,
  Stack,
  Checkbox,
  Button,
  Center,
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { Link } from "wouter";
import { paths } from "config/paths";
import { useSignInForm } from "../hooks/useSignInForm";
import {
  validateEmailSignIn,
  validatePasswordSignIn,
} from "config/validations";
import { CustomButton } from "components/commons/CustomButton";

export function LoginForm() {
  const {
    register,
    handleSubmit,
    errors,
    onSubmit,
    showPass,
    handleShowPass,
    isFetching,
  } = useSignInForm();

  return (
    <>
      <Box my={8} textAlign="left">
        <form>
          <FormControl m={2} isInvalid={errors.email}>
            <FormLabel>Ingresá tu email</FormLabel>
            <Input
              id="email"
              type="email"
              placeholder="Email"
              {...register("email", validateEmailSignIn)}
            />
            <FormErrorMessage>
              {errors.email && errors.email.message}
            </FormErrorMessage>
          </FormControl>

          <FormControl m={2} isInvalid={errors.password}>
            <FormLabel>Ingresá tu contraseña</FormLabel>
            <InputGroup>
              <Input
                id="password"
                type={showPass ? "text" : "password"}
                placeholder="Contraseña"
                {...register("password", validatePasswordSignIn)}
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={handleShowPass}>
                  {showPass ? <ViewOffIcon /> : <ViewIcon />}
                </Button>
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>
              {errors.password && errors.password.message}
            </FormErrorMessage>
          </FormControl>

          <Stack isInline justifyContent="space-between" mt={4}>
            <Box>
              <Checkbox {...register("remindMe")}>Recordarme</Checkbox>
            </Box>
            <Box>
              <Link to={paths.forgetterpass}>
                <LinkChakra color="teal.500">
                  ¿Olvidaste tu contraseña?
                </LinkChakra>
              </Link>
            </Box>
          </Stack>

          <Center m={8}>
            <CustomButton
              handleClick={handleSubmit(onSubmit)}
              type="submit"
              isLoading={isFetching}
              loadingText="Enviando"
              width="60%"
              textButton="Ingresar"
            />
          </Center>
        </form>
      </Box>
    </>
  );
}
