import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { SeccionHeader } from "components/SeccionHeader";
import { LoginForm } from "./LoginForm";
import { sections } from "config/sections";

export function LoginArea() {
  const { login } = sections; 
  
  return (
    <Flex minHeight="440px" width="full" justifyContent="center" mb={8}>
      <Box
        borderWidth={1}
        px={4}
        width="full"
        maxWidth="500px"
        borderRadius={4}
        textAlign="center"
        boxShadow="lg"
      >
        <Box p={4}>
          <SeccionHeader section={login.section} sectionTitle={login.title} />
          <LoginForm />
        </Box>
      </Box>
    </Flex>
  );
}
