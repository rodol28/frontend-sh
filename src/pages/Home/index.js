import React from 'react'

// styles

import styles from './Home.module.css';

// component Home

export function Home() {
    return (
        <div className={styles.container_home}>
            <h2>Home</h2>
        </div>
    )
}
