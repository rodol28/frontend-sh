import { useState, useEffect } from "react";
import { useToast } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";

export function useProfileFormOwner() {
  const dispatch = useDispatch();
  const toast = useToast();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (e) => {
    e.preventDefault();
    console.log("submit");
  };

  const onCancel = (e) => {
    e.preventDefault();
    console.log("Cancel");
  };

  useEffect(() => {}, []);

  return {
    register,
    handleSubmit,
    onSubmit,
    onCancel,
    errors,
  };
}
