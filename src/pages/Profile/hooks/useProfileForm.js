// eslint-disable-next-line
import { useState, useEffect } from "react";
// import { useToast } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
// import { useDispatch, useSelector } from "react-redux";

export function useProfileForm() {
  // const dispatch = useDispatch();
  // const toast = useToast();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log({ data });
    console.log("submit");
    // dispatch(updateStudentData(data));
  };

  const onCancel = (e) => {
    e.preventDefault();
    console.log("Cancel");
  };

  useEffect(() => {}, []);

  return {
    register,
    handleSubmit,
    onSubmit,
    onCancel,
    errors,
  };
}
