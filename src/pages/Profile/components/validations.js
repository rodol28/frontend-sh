  export const validateUsername = {
    required: "El número de nombre de usuario es requerido.",
    maxLength: {
      value: 20,
      message: "El nombre de usuario no puede exceder los 20 caracteres.",
    },
  };
  
  export const validatePhone = {
    required: "El número de teléfono es requerido.",
    pattern: {
      value: /^[0-9\b]+$/,
      message: "Ingresa solo números.",
    },
    minLength: {
      value: 10,
      message: "La longitud mínima del numero de telefono son 10 dígitos.",
    },
    maxLength: {
      value: 11,
      message: "El número de teléfono no puede exceder los 11 caracteres.",
    },
  };
  
  export const validatePassword = {
    required: "La contraseña es requerida.",
    pattern: {
      value: /(?=.*[0-9])/,
      message: "La contraseña debe contener un número.",
    },
    minLength: {
      value: 8,
      message: "La contraseña debe contener al menos 8 caracteres.",
    },
  };
  
  export const validateGender = {
    required: "El genero es requerido.",
  };
  
  export const validateCarreer = {
    required: "La carrera es requerida.",
  };
  
  export const validateState = {
    required: "La provincia es requerida.",
  };
  
  export const validateCity = {
    required: "La ciudad es requerida.",
  };
  