import React from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@chakra-ui/react";
import { SeccionHeader } from "components/SeccionHeader";
import { ProfileForm } from "./ProfileForm";
import { PublicationsList } from "./PublicationsList";
import { sections } from "config/sections";

export function AccountTabs() {
  const { profile } = sections;

  return (
    <>
      <Tabs variant="soft-rounded" colorScheme="gray">
        <TabList>
          <Tab>Publicaciones</Tab>
          <Tab>Mi cuenta</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <SeccionHeader
              section={profile.section}
              sectionTitle={profile.titlePublications}
            />
            <PublicationsList />
          </TabPanel>
          <TabPanel>
            <SeccionHeader
              section={profile.section}
              sectionTitle={profile.titleAccount}
            />
            <ProfileForm />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  );
}
