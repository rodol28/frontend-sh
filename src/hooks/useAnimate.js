import { useEffect } from "react";

function showScroll() {
  let listElement = document.querySelectorAll("#load-animate");
  // console.log(listElement)
  let scrollTop = document.documentElement.scrollTop;
  for (var i = 0; i < listElement.length; i++) {
    let height = listElement[i].offsetTop;
    if (height - 500 < scrollTop) {
      listElement[i].style.opacity = 1;
      listElement[i].classList.add("showAnimate");
    }
  }
}

export function useAnimate() {
  useEffect(() => {
    window.addEventListener("scroll", showScroll);
  }, []);
}
