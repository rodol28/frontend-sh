import { useState, useEffect } from "react";
import { utils } from "services";

export function useGetCities() {
    const [stateSelected, setStateSelected] = useState(1);
    const [cities, setCities] = useState([]);

    useEffect(() => {
        utils.getCityById({ id: stateSelected }).then(res => setCities(res));
    }, [stateSelected]);

    return { cities, setStateSelected };
}