import { useState, useEffect } from "react";
import { utils } from "services";

export function useGetCarreers() {
  const [carreers, setCarreers] = useState([]);

  useEffect(() => {
    utils
      .getCarrers()
      .then(data => setCarreers(data))
      .catch(err => setCarreers([]));
  }, []);

  return { carreers };
}
