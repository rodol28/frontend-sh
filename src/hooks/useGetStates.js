import { useState, useEffect } from "react";
import { utils } from "services";

export function useGetState() {
  const [states, setStates] = useState([]);

  useEffect(() => {
    utils.getAllStatesService().then(res => setStates(res));
  }, []);

  return { states };
}
