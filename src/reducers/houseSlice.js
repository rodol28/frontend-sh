import { createSlice } from "@reduxjs/toolkit";
import { houses, utils } from "services";
import { helpers } from "helpers";

const initialState = {
  houses: [],
  house: null,
  isFetching: false,
  isSuccess: false,
  isError: false,
  errorMessage: "",
};

export const housesSlice = createSlice({
  name: "houses",
  initialState,
  reducers: {
    houseLoading: (state, action) => {
      if (state.isFetching === false) {
        state.isFetching = true;
      }
    },
    successRegistrerHouse: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.isSuccess = true;
      }
    },
    setCurrentHouse: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.house = action.payload;
      }
    },
    resetCurrentHouse: (state, action) => {
      state.house = null;
    },
    fetchFailed: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.error = action.payload;
      }
    },
    clearState: (state, action) => {
      state.isSuccess = false;
      state.isError = false;
      state.errorMessage = "";
    },
  },
});

export const {
  houseLoading,
  successRegistrerHouse,
  setCurrentHouse,
  resetCurrentHouse,
  fetchFailed,
  clearState,
} = housesSlice.actions;

export const registerHouseAction = (data) => async (dispatch) => {
  dispatch(houseLoading());
  try {
    const { data: house, images } = data;
    const body = helpers.mapHouseToApi({ house });

    const idHouse = await houses.postHouseService({ body });

    const formData = new FormData();
    for (let index = 0; index < images.length; index++) {
      formData.append("images", images[index]);
    }
    formData.append("idHouse", idHouse);
    const res = await utils.postImagesService({ formData });

    if (res.status === "Error") {
      dispatch(fetchFailed(res.message));
      return;
    }

    setLastIdRegistered(idHouse);
    dispatch(successRegistrerHouse());
  } catch (err) {
    console.error(err);
    dispatch(fetchFailed(err.toString()));
  }
};

const setLastIdRegistered = (idHouse) => {
  localStorage.setItem("lastId", idHouse);
};

export default housesSlice.reducer;

export const housesSelector = (state) => state.houses;
