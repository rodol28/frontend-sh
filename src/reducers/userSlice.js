import { createSlice } from "@reduxjs/toolkit";
import { users } from "services";
// eslint-disable-next-line
// import { helpers } from "helpers";

const initialState = {
  user: null,
  currentUser: null,
  isFetching: false,
  isSuccess: false,
  isError: false,
  errorMessage: "",
};

export const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    userLoading: (state) => {
      if (state.isFetching === false) {
        state.isFetching = true;
      }
    },
    successGetCurrentUser: (state, action) => {
      state.isFetching = false;
      state.currentUser = action.payload;
      state.isSuccess = true;
    },
    successUpdate: (state, action) => {
      state.isFetching = false;
      state.isSuccess = true;
    },
    failed: (state, action) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = action.payload.error;
    },
    clearState: (state, action) => {
      state.isSuccess = false;
      state.isError = false;
      state.errorMessage = "";
    },
  },
});

export const {
  userLoading,
  successGetCurrentUser,
  successUpdate,
  failed,
  clearState,
} = userSlice.actions;

export const getCurrentUserAction = () => async (dispatch) => {
  dispatch(userLoading());

  try {
    const userId = 1; // AQUI LEERLO DESDE EL TOKEN
    const currentUser = await users.getCurrentUser({ userId });
    // if (statusCode === 201) {
    dispatch(successGetCurrentUser(currentUser));
    // }
  } catch (error) {
    console.log(error.message);
    dispatch(failed(error.message));
  }
};

export const updateUserAction = (data) => async (dispatch) => {
  dispatch(userLoading());
  
  // const body = helpers.mapUserToApi({ user: data });
  try {
    console.log({ userToUpdate: data });
    // const response = await auth.signUpService({ body });
    // eslint-disable-next-line
    // const statusCode = response.status;
    // const res = await response.json();
    
    // if (statusCode === 201) {
      dispatch(successUpdate());
      // }
    } catch (error) {
      console.log(error.message);
      dispatch(failed(error.message));
    }
  };
  
  export default userSlice.reducer;

  export const userSelector = (state) => state.user;
  export const currentUserSelector = (state) => state.user.currentUser;
  