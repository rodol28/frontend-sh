import { createSlice } from "@reduxjs/toolkit";
import { auth } from "services";
import { helpers } from "helpers";

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    isLoggedIn: false,
    user: null,
    token: null,
    isFetching: false,
    isSuccess: false,
    isError: false,
    errorMessage: "",
  },
  reducers: {
    authLoading: state => {
      if (state.isFetching === false) {
        state.isFetching = true;
      }
    },
    successSignIn: (state, action) => {
      state.isFetching = false;
      state.token = action.payload.access_token;
      state.user = action.payload.user;
      state.isLoggedIn = true;
      state.isSuccess = true;
    },
    successSignOut: (state, action) => {
      state.isFetching = false;
      state.user = null;
      state.token = null;
      state.isLoggedIn = false;
      state.isSuccess = false;
      state.isError = false;
      state.errorMessage = "";
    },
    successSignUp: (state, action) => {
      state.isFetching = false;
      state.isSuccess = true;
    },
    authFailed: (state, action) => {
      state.isFetching = false;
      state.isError = true;
      state.errorMessage = action.payload.error;
    },
    clearState: (state, action) => {
      state.isSuccess = false;
      state.isError = false;
      state.errorMessage = "";
    },
  },
});

export const {
  authLoading,
  authFailed,
  successSignIn,
  successSignUp,
  successSignOut,
  clearState,
} = authSlice.actions;

export const signInAction = data => async dispatch => {
  dispatch(authLoading());
  try {
    const { email, password } = data;
    const response = await auth.signInService({ email, password });
    const statusCode = response.status;
    const res = await response.json();

    if (statusCode === 200) {
      dispatch(successSignIn(res));
    }
    if (statusCode === 401) {
      dispatch(authFailed(res));
    }
  } catch (error) {
    dispatch(authFailed(error.message));
  }
};

export const signOutAction = data => async dispatch => {
  dispatch(authLoading());
  try {
    const { token } = data;
    const response = await auth.signOutService({ token });
    const statusCode = response.status;
    const res = await response.json();

    if (statusCode === 200) {
      dispatch(successSignOut(res));
    }
    if (statusCode === 400) {
      dispatch(authFailed(res));
    }
  } catch (error) {
    dispatch(authFailed(error.message));
  }
};

export const signUpAction = data => async dispatch => {
  dispatch(authLoading());

  const body = helpers.mapUserToApi({ user: data });
  try {
    const response = await auth.signUpService({ body });
    // eslint-disable-next-line
    const statusCode = response.status;
    // const res = await response.json();

    // if (statusCode === 201) {
    dispatch(successSignUp());
    // }
  } catch (error) {
    console.log(error.message);
    dispatch(authFailed(error.message));
  }
};

export default authSlice.reducer;

export const authSelector = state => state.auth;
