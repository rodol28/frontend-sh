import { createSlice } from "@reduxjs/toolkit";
import { publications } from "services";
import { helpers } from "helpers";

const initialState = {
  posts: [],
  post: null,
  isFetching: false,
  isSuccess: false,
  isError: false,
  errorMessage: "",
};

export const publicationSlice = createSlice({
  name: "publications",
  initialState,
  reducers: {
    publicationsReceived: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.posts = action.payload;
      }
    },
    setCurrentPublication: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.post = action.payload;
      }
    },
    successCreatePublication: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.isSuccess = true;
      }
    },
    resetCurrentPublication: (state, action) => {
      state.post = null;
    },
    publicationLoading: (state, action) => {
      if (state.isFetching === false) {
        state.isFetching = true;
      }
    },
    fetchFailed: (state, action) => {
      if (state.isFetching === true) {
        state.isFetching = false;
        state.isError = action.payload;
      }
    },
    clearState: (state, action) => {
      state.isSuccess = false;
      state.isError = false;
      state.errorMessage = "";
    },
  },
});

export const {
  publicationsReceived,
  setCurrentPublication,
  successCreatePublication,
  resetCurrentPublication,
  publicationLoading,
  fetchFailed,
  clearState,
} = publicationSlice.actions;

export const fetchAllPublications = () => async (dispatch) => {
  dispatch(publicationLoading());
  try {
    const allPublications = await publications.getAllPublications();
    dispatch(publicationsReceived(allPublications));
  } catch (err) {
    dispatch(fetchFailed(err.toString()));
  }
};

export const fetchPublicationById = (idPost) => async (dispatch) => {
  dispatch(publicationLoading());
  try {
    const publication = await publications.getPublicationById(idPost);
    dispatch(setCurrentPublication(publication));
  } catch (err) {
    dispatch(fetchFailed(err.toString()));
  }
};

export const fetchPublicationsByFilters = (filters) => async (dispatch) => {
  dispatch(publicationLoading());
  try {
    const filteredPublications = await publications.getPublicationsByFilters(
      filters
    );
    dispatch(publicationsReceived(filteredPublications));
  } catch (err) {
    dispatch(fetchFailed(err.toString()));
  }
};

export const createPublicationAction = (data) => async (dispatch) => {
  dispatch(publicationLoading());
  try {
    data.lastRegisteredHouse = getLastIdRegistered();
    const body = helpers.mapPublicationToApi({ publication: data });
    console.log({ body });
    // eslint-disable-next-line
    const result = await publications.postPublication({ body });

    removeLastIdRegistered();
    dispatch(successCreatePublication());
  } catch (err) {
    dispatch(fetchFailed(err.toString()));
  }
};

const getLastIdRegistered = () => {
  return localStorage.getItem("lastId");
};

const removeLastIdRegistered = () => {
  localStorage.removeItem("lastId");
};

export default publicationSlice.reducer;

export const publicationSelector = (state) => state.publications;
