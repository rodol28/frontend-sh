export function signOutService({ token }) {
  return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/auth/logout`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then(response => response)
    .catch(err => console.error(err));
}
