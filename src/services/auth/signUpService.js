export function signUpService({ body }) {
  return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/students`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  })
    .then(response => response)
    .catch(err => console.error(err));
}
