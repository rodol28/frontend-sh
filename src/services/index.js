/* publications */
import { getAllPublications } from "./publications/getAllPublications";
import { getPublicationsByFilters } from "./publications/getPublicationByFilters";
import { getPublicationById } from "./publications/getPublicationById";
import { postPublication } from "./publications/postPublication";

/* houses */
import { postHouseService } from "./houses/postHouse";

/* auth */
import { signInService } from "./auth/signInService";
import { signOutService } from "./auth/signOutService";
import { signUpService } from "./auth/signUpService";

/* utils */
import { getAllStatesService } from "./utils/getAllStates";
import { getCarrers } from "./utils/getCarreers";
import { getCityById } from "./utils/getCityById";
import { postImagesService } from "./utils/postImages";

/* users */
import { getCurrentUser } from "./users/getCurrentUser";

export const publications = {
  getAllPublications,
  getPublicationsByFilters,
  getPublicationById,
  postPublication,
};

export const houses = { postHouseService };

export const auth = { signUpService, signInService, signOutService };

export const utils = {
  getAllStatesService,
  getCityById,
  getCarrers,
  postImagesService,
};

export const users = { getCurrentUser };