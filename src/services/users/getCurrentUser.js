export function getCurrentUser({ userId }) {
  return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/students/${userId}`)
    .then((response) => response.json())
    .then((res) => res)
    .catch((err) => {
      console.error(err);
      return [];
    });
}
