export function getCityById({ id }) {
    return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/cities/${id}`, {
        method: "GET",
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers":
                "Origin, X-Requested-With, Content-Type, Accept",
        },
    })
        .then(response => response.json())
        .then(res => res)
        .catch(err => {
            console.error(err);
            return [];
        });
}