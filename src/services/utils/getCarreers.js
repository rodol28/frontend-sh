export function getCarrers() {
  return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/carrers`)
    .then(response => response.json())
    .then(res => res)
    .catch(err => {
      console.error(err);
      return [];
    });
}
