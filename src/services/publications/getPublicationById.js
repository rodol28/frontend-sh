export function getPublicationById({ idPost }) {
    return fetch(`${process.env.REACT_APP_API_TEST_POST}/${idPost}`)
        .then(response => response.json())
        .then(res => res)
        .catch(err => {
            console.error(err);
            return [];
        });
}
