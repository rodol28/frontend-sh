export function getPublicationsByFilters({ filters }) {
  return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/publications`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(filters),
  })
    .then(response => response.json())
    .then(res => res)
    .catch(err => {
      console.error(err);
      return [];
    });
}
