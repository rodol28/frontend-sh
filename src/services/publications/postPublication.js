export function postPublication({ body }) {
    return fetch(`${process.env.REACT_APP_API_LARAVEL_URL}/publications`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((res) => res.id)
      .catch((err) => console.error(err));
  }
  