export function getAllPublications() {
  return fetch(`${process.env.REACT_APP_API_TEST_POST}`)
    .then(response => response.json())
    .then(res => res)
    .catch(err => {
      console.error(err);
      return [];
    });
}
