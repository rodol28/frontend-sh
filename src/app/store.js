import { configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import publications from "reducers/publicationSlice";
import authReducer from "reducers/authSlice";
import houseSlice from "reducers/houseSlice";
import userSlice from "../reducers/userSlice";

const reducers = combineReducers({
  publications: publications,
  auth: authReducer,
  houses: houseSlice,
  user: userSlice
});

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== "production",
  middleware: [thunk],
});

export default store;
