import React from "react";
import { Route, Switch } from "wouter";

// import Pages

import { Main } from "pages/Main";
import { AboutUs } from "pages/AboutUs";
import { Home } from "pages/Home";
import { SignIn } from "pages/SignIn";
import { SignUp } from "pages/SignUp";
import { Profile } from "pages/Profile";
import { ForgotterPass } from "pages/ForgotterPass";
import { Search } from "pages/Search";
import { PostDetail } from "pages/PostDetail";

/* owners */
import { RegisterHousePage } from "pages/owners/RegisterHouse";
import { RegisterPublicationPage } from "pages/owners/RegisterPublication";
import { ListPublications } from "pages/owners/ListPublications";

// paths
import { paths } from "config/paths";

// component Routes

export default function Routes() {
  return (
    <Switch>
      <Route path={paths.main} component={Main} exact />
      <Route path="/home" component={Home} exact />
      <Route path={paths.aboutUs} component={AboutUs} exact />
      <Route path={paths.login} component={SignIn} exact />
      <Route path={paths.register} component={SignUp} exact />
      <Route path={paths.account} component={Profile} exact />
      <Route path={paths.forgetterpass} component={ForgotterPass} exact />
      <Route path={paths.search} component={Search} exact />
      <Route path="/publicaciones/detalle/:id" component={PostDetail} exact />

      <Route path={paths.postList} component={ListPublications} exact />
      <Route path={paths.registerHouse} component={RegisterHousePage} exact />
      <Route
        path={paths.registerPost}
        component={RegisterPublicationPage}
        exact
      />
      <Route>404, Not Found!</Route>
    </Switch>
  );
}
