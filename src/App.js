import "./App.css";
import Routes from "./routes";
import { NavBar } from "components/Header";
import { Footer } from "components/commons/Footer";

function App() {
  return (
    <div className="App">
      <NavBar />
      <Routes />
      <Footer />
    </div>
  );
}

export default App;
